/*
События клавиатуры предназначены для клавиатуры. Их можно использовать для проверки input, но текст может быть вставлен
мышкой или другим способом
*/

const allBtn = document.querySelectorAll('.btn');

function lightingBtn (el) {
    el.classList.add('active');
}

function removeActive () {
    for (let i = 0; i < allBtn.length; i++) {
        allBtn[i].classList.remove('active');
    }
}



document.addEventListener('keydown', function (e) {
    removeActive();
    for (let i = 0; i < allBtn.length; i++) {
        if (allBtn[i].innerHTML.toUpperCase() === e.key.toUpperCase()) {
            lightingBtn(allBtn[i]);
            return;
        }
    }
});